from flask import Flask, render_template


app = Flask(__name__)

@app.route('/')
def index():
    return '<p>Hello, World!</p>'

@app.route('/rendertemplate')
def rendertemplate():
    return render_template('demo_rendertemplate.html')

SOME_GLOBAL_VAR='abb'
@app.route('/rendertemplatewithparam')
def rendertemplatewithparam():
    some_local_var=122
    return render_template('demo_rendertemplate_w_param.html', **locals(), **globals())
