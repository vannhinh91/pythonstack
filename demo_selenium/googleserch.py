from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager


WD_IMPLICITLY_WAIT=6

def load_webdriver_f_autoinstall_chrome_wd():  # f aka from
    #region bypass permission popup ref. https://stackoverflow.com/a/39997484/248616  ref2. https://www.programcreek.com/python/example/100025/selenium.webdriver.ChromeOptions
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-notifications")
    #endregion

    wd = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)

    wd.implicitly_wait(WD_IMPLICITLY_WAIT)
    wd.maximize_window()
    return wd


wd = load_webdriver_f_autoinstall_chrome_wd()  # wd aka webdriver

try:
    wd.get('https://google.com')
    # wd.get('https://stackoverflow.com')  #TODO scrape :stackoverflow after :googlesearch
    pass

except:
    raise

finally:
    wd.quit()  # CAUTION quit called is a MUST to close webdriver instance and free machine resource
